Zapstreak HelloWorld Application
====================

This is a simple starting point for writting applications using the
[Zapstreak](http://zapstreak.com "Zapstreak Library") 'stream to anything' library.

Feel free to download it and modify in any way you'd like.

More docummentation can be found in the [Integration Guideline](http://dist.zapstreak.com/final/2.2.1/Integration%20Guideline.pdf).
