package com.zapstreak.helloworld;

import java.util.LinkedHashSet;
import java.util.Set;

import com.fusionsheep.dlna.RendererDevice;
import com.fusionsheep.dlna.RendererFinder;
import com.fusionsheep.dlna.RendererFinder.LicenseExpiredException;
import com.fusionsheep.dlna.RendererFinder.LicenseFirstConnectionException;
import com.fusionsheep.dlna.RendererFinder.LicenseNetworkMissingException;
import com.fusionsheep.dlna.RendererFinder.LicenseNumberException;
import com.fusionsheep.dlna.RendererFinder.OnRendererFinderListener;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;


public class MainActivity extends Activity {
    private static final String TAG = MainActivity.class.getCanonicalName();

    // This object is the entrance point for the whole Zapstreak library.
    private RendererFinder mFinder = null;

    // Dynamically updated list of all the devices we've found so far.
    private final Set<ZapstreakDevice> devices = 
            new LinkedHashSet<ZapstreakDevice>();
    
    private ZapstreakDevice zapstreakDevice = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    
    @Override
    protected void onResume() {
        super.onResume();
     
        try {
			mFinder = RendererFinder.instance(getBaseContext());
		} catch (LicenseExpiredException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (LicenseNumberException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (LicenseNetworkMissingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (LicenseFirstConnectionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        if (mFinder == null) {
			return;
		}

        
        mFinder.setOnRendererFindListener(new OnRendererFinderListener() {
			
			@Override
			public void onFind(RendererDevice device) {
				Log.i(TAG, "Found a new device: " + device.getFriendlyName());
                zapstreakDevice = new ZapstreakDevice(MainActivity.this, device);
                // Add the newly found device to the list.
                if(!devices.contains(zapstreakDevice))
                {
                	Log.i(TAG, "Device added: " + zapstreakDevice.device.getFriendlyName());
	                devices.add(zapstreakDevice);
	                zapstreakDevice.action.playRemote("http://zapstreak.com/zapstreak.mp4");
                }
			}
		});
        
        mFinder.findStart();
    }
    
    @Override
    protected void onPause() {
        for (ZapstreakDevice device: devices) {
            device.action.stop();
        }

        devices.clear();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
    	if (mFinder != null) {
        	mFinder.findStop();			
		}

        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
}
