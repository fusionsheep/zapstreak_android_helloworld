package com.zapstreak.helloworld;

import android.content.Context;
import android.util.Log;

import com.fusionsheep.dlna.RendererAction;
import com.fusionsheep.dlna.RendererDevice;
import com.fusionsheep.dlna.RendererAction.OnRendererActionListener;
import com.fusionsheep.dlna.RendererAction.OnRendererVolumeListener;


public class ZapstreakDevice {
    private static final String TAG = ZapstreakDevice.class.getCanonicalName();

    public final RendererDevice device;
    private final Context mContext;
    
    public final RendererAction action;
    
    public ZapstreakDevice(Context context, RendererDevice device) {
        mContext = context;
        this.device = device;

        this.action = new RendererAction(mContext, this.device);
        this.action.setOnRendererAction(ACTION_LISTENER);
        this.action.setOnVolumeAction(VOLUME_LISTENER);
    }
    
    // Action status listener.
    private static final OnRendererActionListener ACTION_LISTENER =
            new OnRendererActionListener()
    {
		@Override
		public void onInvalidPathOrUrl(RendererDevice device) {
			Log.e(TAG, device.getFriendlyName() +
                    ": The URL or path of the file you are trying to play" +
                    "is invalid");
		}

		@Override
		public void onNoMediaPresent(RendererDevice device) {
			Log.e("TAG", device.getFriendlyName() + ": No media present");
			
		}

		@Override
		public void onPaused(RendererDevice device) {
			Log.i(TAG, device.getFriendlyName() + ": Paused playback");
		}

		@Override
		public void onPlayPosition(RendererDevice device, int iRealTime, String szRealTime,
				int iTrackDuration, String szTrackDuration) {
			Log.v(TAG, new StringBuilder(device.getFriendlyName())
            .append(": Media metadata — current: ")
            .append(szRealTime)
            .append(", duration: ")
            .append(szTrackDuration)
            .toString());
		}

		@Override
		public void onPlaying(RendererDevice device) {
			Log.i(TAG, device.getFriendlyName() + ": Playing media file");
		}

		@Override
		public void onStopped(RendererDevice device) {
			Log.i(TAG, device.getFriendlyName() + ": Stopped playback");			
		}

		@Override
		public void onTransitioning(RendererDevice device) {
			Log.v(TAG, device.getFriendlyName() + ": Preloading file on the TV");			
		}

		@Override
		public void onUnableToPlay(RendererDevice device) {
			Log.e(TAG, device.getFriendlyName() + 
                    ": Device is unable to play this file");
			
		}       
    };

    // Volume listener
    private static final OnRendererVolumeListener VOLUME_LISTENER =
            new OnRendererVolumeListener()
    {
        @Override
        public void onVolumeSet(RendererDevice device, int iVolume)
        {
            Log.d(TAG, device.getFriendlyName() + ": Set volume to " + iVolume);
        }

        @Override
        public void onVolumeGet(RendererDevice device, int iVolume)
        {
            // Here you will get volume level after calling action.volumeGet()
            Log.d(TAG, device.getFriendlyName() +
                    ": The current volume is: " + iVolume);
        }        
    };
}
